#!/bin/bash

set -e

# This script implements the creation of a batch of folders
# as well as anonymous upload share links
# https://docs.nextcloud.com/server/latest/developer_manual/client_apis/OCS/ocs-share-api.html

# Default server
# This is the hostname
NEXTCLOUD_HOST=ergapilot.bsc.es
# This is the base URL, needed to build queries
NEXTCLOUD_BASEURL=/

# The destination relative path where the folders are going to be
# created, ending in a slash
NEXTCLOUD_PATH="<yourpath>/"

# You are adviced to create an app password
# There you will get your username
# and the app password to put below
NEXTCLOUD_USER="<youruser>"
NEXTCLOUD_PASS="<yourpassword>"

# The subdirectories to be created inside the skeleton
declare -a DEFAULT_REMOTE_SKEL_DIRS=(
	Permits
	Images
	Misc
)

retries=5


applyConfig() {
	local nextcloud_host="$1"
	local nextcloud_baseurl="$2"
	local nextcloud_path="$3"
	
	case "${nextcloud_baseurl}" in
		*/)
			# Nothing to be done
			true
			;;
		*)
			# The base url, ending in a slash
			nextcloud_baseurl="${nextcloud_baseurl}/"
	esac
	
	case "${nextcloud_path}" in
		*/)
			# Nothing to be done
			true
			;;
		*)
			# The destination relative path, ending in a slash
			nextcloud_path="${nextcloud_path}/"
	esac
	
	# In case it is undefined
	declare -p | grep -qF ' REMOTE_SKEL_DIRS' || {
		declare -a REMOTE_SKEL_DIRS=( "${DEFAULT_REMOTE_SKEL_DIRS[@]}" )
	}
	
	# Setting up variables
	NEXTCLOUD_SERVER="https://${nextcloud_host}${nextcloud_baseurl}"
	NEXTCLOUD_SERVER_DAV="${NEXTCLOUD_SERVER}remote.php/dav/"
	NEXTCLOUD_SERVER_OCS="${NEXTCLOUD_SERVER}ocs/v2.php/"
	NEXTCLOUD_SERVER_OCS_SHARE="${NEXTCLOUD_SERVER_OCS}apps/files_sharing/api/v1/"
	NEXTCLOUD_SERVER_SHARES="${NEXTCLOUD_SERVER}index.php/s/"
	NEXTCLOUD_PATH="${nextcloud_path}"
	
	if [ -z "$DEBUG" ] ; then
		PRODFLAGS="-f"
	else
		PRODFLAGS="-i"
	fi
}

app_pass_pat='>([^<]+)<'

# https://docs.nextcloud.com/server/latest/developer_manual/client_apis/LoginFlow/index.html#converting-to-app-passwords
authenticate() {
	local user="$1"
	local pass="$2"
	
	local tempauth="$(mktemp -t NC-XXXXXXXX-APP.xml)"
	trap "rm -f $tempauth" INT TERM EXIT
	local authcode="$(curl -s --write-out %{json} -o "${tempauth}" -u "${user}":"${pass}" -H 'OCS-APIRequest: true' "${NEXTCLOUD_SERVER_OCS}"core/getapppassword | grep -o '"http_code":[0-9]*' | cut -f 2 -d ':')"
	
	local retpass="$pass"
	case "$authcode" in
		200)
			# Rescue the password from the temp file
			local appp="$(grep -o '<apppassword>.*</apppassword>' "$tempauth")"
			if [[ "$appp" =~ $app_pass_pat ]] ; then
				retpass="${BASH_REMATCH[1]}"
			fi
			;;
		403)
			# Use the very same password
			true
			;;
		412)
			# Use the very same password
			true
			;;
		*)
			# Either the password is not correct, or two
			# factor authentication has to be used
			echo "FATAL ERROR: http code $authcode while authenticating" 1>&2
			retpass=''
			;;
	esac
	
	echo -n "$retpass"
}




recursiveMkCol() {
	local remotePath="$1"
	local retries="$2"
	
	local -a components
	IFS='/' read -r -a components <<< "${remotePath}"
	
	local remoteFolder="${NEXTCLOUD_SERVER_DAV}files/${NEXTCLOUD_USER}/"
	local comp
	
	local fullRemoteFolder="$remoteFolder"
	for comp in "${components[@]}" ; do
		fullRemoteFolder="${fullRemoteFolder}${comp}/"
	done
	# Check the path, and do the heavy lifting only if it is needed
	curl --retry "${retries}" -s -S -f -k -X PROPFIND -u "${NEXTCLOUD_USER}:${NEXTCLOUD_PASS}" "${fullRemoteFolder}" >& /dev/null || {
		for comp in "${components[@]}" ; do
			remoteFolder="${remoteFolder}${comp}/"
			curl --retry "${retries}" -s -S -f -k -X PROPFIND -u "${NEXTCLOUD_USER}:${NEXTCLOUD_PASS}" "${remoteFolder}" >& /dev/null || ( echo "Creating ${remoteFolder}" && curl --retry "${retries}" -s -S ${PRODFLAGS} -k -X MKCOL -u "${NEXTCLOUD_USER}:${NEXTCLOUD_PASS}" "${remoteFolder}" )
		done
	}
}


listShares() {
	local reqpath="${NEXTCLOUD_SERVER_OCS_SHARE}shares"
	if [ $# -ne 0 ] ; then
		local path="$1"
		local subfiles="$2"
		if [ -z "$subfiles" ] ; then
			subfiles=false
		fi
		reqpath="${reqpath}?subfiles=${subfiles}&path=${path}"
	fi
	
	curl -s -u "${NEXTCLOUD_USER}":"${NEXTCLOUD_PASS}" -H 'OCS-APIRequest: true' "$reqpath"
}


createShare() {
	local -a createParams=(
		path="$1"
		permissions=15
		publicUpload=true
	)
	
	local email="$2"
	local shareType
	if [ -n "$email" ] ; then
		createParams+=( shareWith="$email" )
		shareType=4
	else
		shareType=3
	fi
	
	createParams+=(
		shareType="${shareType}"
	)
	
	local -a dataParams
	local cpar
	for cpar in "${createParams[@]}" ; do
		dataParams+=(
			--data "$cpar"
		)
	done
	
	curl -s "${dataParams[@]}" -u "${NEXTCLOUD_USER}":"${NEXTCLOUD_PASS}" -H 'OCS-APIRequest: true' "${NEXTCLOUD_SERVER_OCS_SHARE}shares"
}


urlencode() {
	python -c "from __future__ import print_function; import urllib.parse, sys; print(urllib.parse.quote(sys.argv[1]))" "$1"
}

curlVerPat='curl ([0-9]+)\.([0-9]+)\.([0-9]+)'
pyVerPat='Python ([0-9]+)\.([0-9]+)\.([0-9]+)'
checkPrereqs() {
	local curlver="$(curl -V|grep -o 'curl [0-9.]*' || true)"
	local curlok
	if [[ "$curlver" =~ $curlVerPat ]] ; then
		if [ "${BASH_REMATCH[1]}" -eq 7 ] ; then
			if [ "${BASH_REMATCH[2]}" -ge 77 ] ; then
				curlok=1
			fi
		elif [ "${BASH_REMATCH[1]}" -gt 7 ] ; then
			curlok=1
		fi
	fi
	
	if [ -z "$curlok" ] ; then
		echo "ERROR: At least curl 7.77.0 is required, and findable in PATH. Found version is $curlver"
		echo "You can fetch an static copy of curl from https://github.com/moparisthebest/static-curl/releases"
		exit 1
	else
		echo "Detected $curlver"
	fi
	
	local pyver="$(python -V|grep -o 'Python [0-9.]*' || true)"
	local pyok
	if [[ "$pyver" =~ $pyVerPat ]] ; then
		if [ "${BASH_REMATCH[1]}" -eq 3 ] ; then
			pyok=1
		fi
	fi
	
	if [ -z "$pyok" ] ; then
		echo "ERROR: At least Python 3.x is required, and findable in PATH. Found version is $pyver"
		exit 2
	else
		echo "Detected $pyver"
	fi
}

checkPrereqs

urlXmlPat='<url>(.*)</url>'
tokenXmlPat='<token>(.*)</token>'

if [ $# -gt 1 ]; then
	configFile="$1"
	shift
	
	if [ -f "$configFile" ] ; then
		source "$configFile"
	else
		echo "ERROR: Configuration file $configFile not found!" 1>&2
		exit 1
	fi
	
	applyConfig "${NEXTCLOUD_HOST}" "${NEXTCLOUD_BASEURL}" "${NEXTCLOUD_PATH}"
	
	# Setting the app password
	NEW_NEXTCLOUD_PASS="$(authenticate "${NEXTCLOUD_USER}" "${NEXTCLOUD_PASS}")"
	
	if [ -z "$NEW_NEXTCLOUD_PASS" ] ; then
		echo "FATAL ERROR: Credentials do not work"
		exit 1
	fi
	
	if [ "${NEW_NEXTCLOUD_PASS}" != "${NEXTCLOUD_PASS}" ] ; then
		echo "INFO: Generated app password $NEW_NEXTCLOUD_PASS . You should update $configFile with it"
		NEXTCLOUD_PASS="${NEW_NEXTCLOUD_PASS}"
	fi
	
	echo "Raw report of current shares over ${NEXTCLOUD_PATH}"
	listShares "${NEXTCLOUD_PATH}" true
	
	idsFile="$1"
	shift
	reportFile="$1"
	if [ -n "$reportFile" ] ; then
		echo Writing tabular report to "$reportFile"
		exec 3> "$reportFile"
	else
		exec 3> /dev/null
	fi
	# Let's process the file with ids
	echo "* Reading ids from $idsFile"
	while IFS=$'\t' read -r LINE email ; do
		case "$LINE" in
			'#'*)
				# Ignore this comment line
				continue
				;;
			*)
				echo -e "  - Processing id $LINE"
				retpath="${NEXTCLOUD_PATH}$(urlencode "$LINE")"
				recursiveMkCol "${retpath}" "${retries}"
				
				# And now, populate
				for subd in "${REMOTE_SKEL_DIRS[@]}" ; do
					recursiveMkCol "${retpath}/${subd}" "${retries}"
				done
				
				shareLink="$(listShares "${retpath}" | grep -o '<\(url\|token\)>.*</\(url\|token\)>' || true)"
				if [ -z "$shareLink" ] ; then
					if [ -n "$email" ] ; then
						echo -e "  - Sending e-mail with the link to $email"
					fi
					shareLink="$(createShare "${retpath}" "$email" || true)"
				fi
				
				if [ -n "$shareLink" ] ; then
					if [ -n "$email" ] ; then
						if [[ "$shareLink" =~ $tokenXmlPat ]] ; then
							shareLink="${NEXTCLOUD_SERVER_SHARES}${BASH_REMATCH[1]} (sent to ${email})"
						else
							shareLink=+" (sent to ${email})"
						fi
					elif [[ "$shareLink" =~ $urlXmlPat ]] ; then
						shareLink="${BASH_REMATCH[1]}"
					fi
					printf "%s\t%s\n" "${LINE}" "${shareLink}"
					printf "%s\t%s\n" "${LINE}" "${shareLink}" 1>&3
				else
					echo "  - There were problems processing $LINE"
				fi
				# This is needed to avoid using the e-mail address on next calls
				unset email
				;;
		esac
	done < "$idsFile"
else
	cat <<EOF
Usage: bash $0 {setup_file} {file with ids} [{report file}]
EOF
fi
