# NextCloud batch share directories creator

## Introduction

This repo contains the script developed to batch create series of directories which have a common parent directory in Nextcloud, having each one of them empty subdirectories like `Misc`, `Images` or `Permits`. For each one of those directories (but not their subdirectories) either a public share link or a share link sent by e-mail will be generated.

## Dependencies

This script needs next command line dependencies:

* bash

* coreutils (`printf`, `grep`, etc...)

* curl 7.77.0 or later (in order to properly gather the http return code in the JSON format with all the gathered details)

* Python 3.x (for url encoding, _sigh_)

## Setup file
A setup file like the [template one provided in this repo](setup.ini.template) should be created with the server, the destination path
of all the directories (it will be created when it does not exist), the empty subdirectories
to create inside each directory of the series and the credentials.

## Usage

```
bash ./ncBatchShareCreator.bash {setup_file} {file with ids} [{tabular report file}]
```

The file with ids can contain lines starting with `#`, which will be discarded.

The file with ids will have a line for each one of the directories to be created whose names will be each one of the ids. The content of the line will be used as is, so beware of spaces and other characters. Slashes are considered as directory separators.

If a line contains two columns separated by a tabulator, the first column is the id and the second column is the e-mail address to send the link to.

The tabular report file is a tabular two columns file. The first column is the id, and the second one is the generated share link.
